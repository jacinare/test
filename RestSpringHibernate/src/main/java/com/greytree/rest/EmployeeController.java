package com.greytree.rest;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.greytree.spring.dao.MyCustomException;
import com.greytree.spring.model.Employee;
import com.greytree.spring.service.EmployeeService;

@Component
@Path("/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@GET
	@Path("/list")
	public Response listEmployee() {	
		return Response.status(200).entity(converToJson(employeeService.findAllEmployees())).build();
	}
	
	@GET
	@Path("/update")
	public Response updateEmployee() {
		try {
			employeeService.updateEmployee(new Employee());
		} catch (MyCustomException e) {
			return Response.status(404).entity("error trying to update user").build();
		}
		return Response.status(200).entity("ok").build();
	}

	@GET
	@Path("/createguay")
	@Produces("text/html")
	public Response createGuay(@QueryParam("pname") String nombre, @QueryParam("psalary") BigDecimal salario,
			 @QueryParam("pssn") String ssn){
		
		Employee employee = new Employee();
		employee.setName(nombre);
		employee.setSalary(salario);
		employee.setJoiningDate(new LocalDate());
		employee.setSsn(ssn);

		employeeService.saveEmployee(employee);
		return Response.status(200).entity("employee created").build();
	}
	
	@GET
	@Path("/create")
	public Response createEmployee() {
		
        /*
         * Create Employee1
         */
        Employee employee1 = new Employee();
        employee1.setName("Guiri Pink");
        employee1.setJoiningDate(new LocalDate(2010, 10, 10));
        employee1.setSalary(new BigDecimal(1));
        employee1.setSsn("ssn00000001");
 
        /*
         * Create Employee2
         */
        Employee employee2 = new Employee();
        employee2.setName("Guiri blue");
        employee2.setJoiningDate(new LocalDate(2012, 11, 11));
        employee2.setSalary(new BigDecimal(20000));
        employee2.setSsn("ssn00000002");
 
        /*
         * Persist both Employees
         */
        employeeService.saveEmployee(employee1);
        employeeService.saveEmployee(employee2);
        
		return Response.status(200).entity("pink and blue created").build();

	}
	
	@GET
	@Path("/deletepink")
	public Response deletePink () {
		employeeService.deleteEmployeeBySsn("ssn00000001");
		return Response.status(200).entity("pink deleted").build();
	}
	
	@GET
	@Path("/deleteblue")
	public Response deleteBlue () {
		employeeService.deleteEmployeeBySsn("ssn00000002");
		return Response.status(200).entity("blue deleted").build();
	}

	private String converToJson(Object employees) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString="";
		try {
			jsonInString = mapper.writeValueAsString(employees);
		} catch (IOException e) {
			System.out.println("Error converting to json..");
		}
		return jsonInString;
	}
}
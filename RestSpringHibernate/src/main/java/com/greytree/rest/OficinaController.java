package com.greytree.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.greytree.spring.dao.MyCustomException;
import com.greytree.spring.model.Oficina;
import com.greytree.spring.service.OficinaService;

@Component
@Path("/oficina")
public class OficinaController {

	@Autowired
	OficinaService oficinaService;

	@GET
	@Path("/list")
	public Response listOficina() {

		String result = "";
		List<Oficina> oficinas = oficinaService.findAllOficina();
		for (Oficina ofi : oficinas) {
			result = result + ofi + "\n";
		}
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/update")
	public Response updateOficina() {

		try {
			oficinaService.updateOficina(new Oficina());
		} catch (MyCustomException e) {
			return Response.status(404).entity("error trying to update Oficina").build();
		}
		return Response.status(200).entity("ok").build();
	}

	@GET
	@Path("/create")
	public Response createOficina() {

		Oficina oficina1 = new Oficina();
		oficina1.setName("Gestoria");
		oficina1.setAdress("C/ Velazquez 1");
		oficina1.setPostcode("11360");
		oficina1.setCreation_date(new LocalDate(2016, 01, 01));

		Oficina oficina2 = new Oficina();
		oficina2.setName("Shop");
		oficina2.setAdress("233 Shields Road");
		oficina2.setPostcode("NE1 QW2");
		oficina2.setCreation_date(new LocalDate(2017, 04, 06));

		oficinaService.saveOficina(oficina1);
		oficinaService.saveOficina(oficina2);

		return Response.status(200).entity("Gestoria and Shop created").build();
	}

	@GET
	@Path("/delegestoria")
	public Response deleteGestoria() {
		oficinaService.DeleteOficinaById(1);
		return Response.status(200).entity("Gestoria deleted").build();
	}

	@GET
	@Path("/deleshop")
	public Response deleteShop() {
		oficinaService.DeleteOficinaById(2);
		return Response.status(200).entity("Shop deleted").build();
	}
}

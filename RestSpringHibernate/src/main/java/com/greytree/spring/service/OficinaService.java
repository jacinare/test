package com.greytree.spring.service;

import java.util.List;

import com.greytree.spring.dao.MyCustomException;
import com.greytree.spring.model.Oficina;

public interface OficinaService {
	
	void saveOficina(Oficina oficina);

	List<Oficina> findAllOficina();

	void DeleteOficinaById(int id);

	Oficina findById(int id);

	void updateOficina(Oficina oficina) throws MyCustomException;
}
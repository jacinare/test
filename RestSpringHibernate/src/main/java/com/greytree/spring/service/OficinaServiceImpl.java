package com.greytree.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greytree.spring.dao.MyCustomException;
import com.greytree.spring.dao.OficinaDao;
import com.greytree.spring.model.Oficina;

@Service
@Transactional
public class OficinaServiceImpl implements OficinaService {

	@Autowired
	private OficinaDao dao;

	public void saveOficina(Oficina oficina) {
		dao.saveOficina(oficina);
	}

	public List<Oficina> findAllOficina() {
		return dao.findAllOficina();
	}

	public void DeleteOficinaById(int id) {
		dao.DeleteOficinaById(id);
	}

	public Oficina findById(int id) {
		return dao.findById(id);
	}

	public void updateOficina(Oficina oficina) throws MyCustomException {
		dao.updateOficina(oficina);
	}
}

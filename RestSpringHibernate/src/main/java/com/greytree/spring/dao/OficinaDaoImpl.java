package com.greytree.spring.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.greytree.spring.model.Oficina;

@Repository("oficinaDao")
public class OficinaDaoImpl extends AbstractDao implements OficinaDao {

	public void saveOficina(Oficina oficina) {	
		
		persist(oficina);
	}

	@SuppressWarnings("unchecked")
	public List<Oficina> findAllOficina() {
		
		Criteria criteria = getSession().createCriteria(Oficina.class);
		return (List<Oficina>) criteria.list();
	}

	public void DeleteOficinaById(int id) {
		
		Query query = getSession().createSQLQuery("delete from OFICINA where id = :id");
		query.setLong("id", id);
		query.executeUpdate();
	}

	public Oficina findById(int id) {
		
		Criteria criteria = getSession().createCriteria(Oficina.class);
		criteria.add(Restrictions.eq("id", id));
		return (Oficina) criteria.uniqueResult();
	}

	public void updateOficina(Oficina oficina) throws MyCustomException {
		
		try {
			getSession().update(oficina);
		} catch(Exception e) {
			throw new MyCustomException();
		}
	}
}
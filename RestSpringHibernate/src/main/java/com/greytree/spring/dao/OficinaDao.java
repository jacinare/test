package com.greytree.spring.dao;

import java.util.List;

import com.greytree.spring.model.Oficina;

public interface OficinaDao {

	void saveOficina(Oficina oficina);

	List<Oficina> findAllOficina();

	void DeleteOficinaById(int id);

	Oficina findById(int id);

	void updateOficina(Oficina oficina) throws MyCustomException;

}

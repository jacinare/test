package com.greytree.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

@Entity
@Table(name = "OFICINA")
public class Oficina {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "ADRESS", nullable = false)
	private String adress;

	@Column(name = "POST_CODE", nullable = false)
	private String post_code;

	@Column(name = "CREATION_DATE", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate creation_date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPostcode() {
		return post_code;
	}

	public void setPostcode(String postcode) {
		this.post_code = postcode;
	}

	public LocalDate getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(LocalDate creation_date) {
		this.creation_date = creation_date;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Oficina))
			return false;
		Oficina other = (Oficina) obj;
		if (id != other.id)
			return false;
		return true;
	}

	// @Override
	// public int hashCode() {
	// // TODO Auto-generated method stub
	// return super.hashCode();
	// }

	@Override
	public String toString() {

		return "Oficina [id=" + id + ", name = " + name + ", adress = " + adress + ", postcode = " + post_code
				+ ", creation_date = " + creation_date + "]";
	}

}

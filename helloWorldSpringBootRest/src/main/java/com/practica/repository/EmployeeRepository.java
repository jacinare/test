package com.practica.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.practica.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    List<Employee> findByName(String name);
    List<Employee> findBySsn(String ssn);
    List<Employee> findByIdInAndName (List<Long> idList, String name);
    List<Employee> findByNameAndSsn(String name, String ssn);
    List<Employee> findByNameNot(String name);
}
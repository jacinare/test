package com.practica.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.practica.model.Oficina;

public interface OficinaRepository extends CrudRepository<Oficina, Long> {
	
	List<Oficina> findById(Long id);
	List<Oficina> findByName(String name);
	List<Oficina> findByAdress (String adress);
	List<Oficina> findByPostCode (String postCode);
	List<Oficina> findByCreationDate (String creationDate);
	
}

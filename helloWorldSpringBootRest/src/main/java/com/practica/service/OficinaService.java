package com.practica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practica.model.Oficina;
import com.practica.repository.OficinaRepository;

@Service
public class OficinaService {

	@Autowired
	private OficinaRepository oficinaRepository;
	
	public String saveOficina(Oficina oficina){
		oficinaRepository.save(oficina);
		String frase = "Oficina saved";
		return frase;
	}
	
	public List<Oficina> findAllOficina (){	
		return (List<Oficina>) oficinaRepository.findAll();
	}
	
	public String deleteOficinaByName (String name){
		for(Oficina oficinaByName: oficinaRepository.findByName(name)){
			oficinaRepository.delete(oficinaByName);
		}
		String frase = "Oficina deleted";
		return frase;
	}
	
	public String updateOficina(Oficina oficina){
		
		for (Oficina oficinaById : oficinaRepository.findById(oficina.getId())){
			
			oficinaById.setName(oficina.getName());
			oficinaById.setAdress(oficina.getAdress());
			oficinaById.setPostCode(oficina.getPostCode());
			oficinaRepository.save(oficinaById);
		}
 	
		String frase = "Oficina updated";
		return frase;
	}
}

package com.practica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practica.model.Employee;
import com.practica.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	
	public String saveEmployee(Employee employee) {

		employeeRepository.save(employee);
		String frase = "Employee saved";
		return frase;
	}

	public List<Employee> findAllEmployees() {
		
		return (List<Employee>) employeeRepository.findAll();
	}
	
	public String deleteEmployeeById(Long id) {

		employeeRepository.delete(id);
		String frase = "Employee deleted";
		return frase;
	}
	
	public String deleteEmployeeByName(String name) {

		for (Employee employeeByName : employeeRepository.findByName(name)){
			employeeRepository.delete(employeeByName);
		}
		String frase = "Employee deleted";
		return frase;
	}
	
	public String deleteEmployeeBySsn(String ssn){
		
		employeeRepository.delete(employeeRepository.findBySsn(ssn));
		String frase = "Employee deleted";
		return frase;
	}
	
	public String updateEmployee(Employee employee) {
		
		for (Employee employeeBySsn : employeeRepository.findBySsn(employee.getSsn())){
			employeeBySsn.setName(employee.getName());
			employeeBySsn.setJoiningDate(employee.getJoiningDate());
			employeeBySsn.setSalary(employee.getSalary());
			employeeRepository.save(employeeBySsn);
		}
		String frase = "Employee updated";
		return frase;
	}
	
}

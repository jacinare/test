package com.practica.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practica.model.Employee;
import com.practica.service.EmployeeService;
;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping("/findAll")
	public List<Employee> findAll(){
		return employeeService.findAllEmployees();
	}
	
	@RequestMapping(value="/save", method=RequestMethod.GET)
	public String saveEmployee(
			@RequestParam(value="name", required=false) String name, 
			@RequestParam(value="salary", required=false) Double salary, 
			@RequestParam(value="ssn", required=false) String ssn){
		
		Employee employee = new Employee();
		employee.setName(name);
		employee.setSalary(salary);
		employee.setJoiningDate(LocalDate.now());
		employee.setSsn(ssn);
		return employeeService.saveEmployee(employee);
	}
	
	@RequestMapping(value = "/deleteById/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteById(@PathVariable("id") Long id) {
		employeeService.deleteEmployeeById(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping("/deleteByName")
	public String deleteByName(@RequestParam(value="name", required=false) String name){
		
		return employeeService.deleteEmployeeByName(name);
	}
	
	@RequestMapping("/deleteBySsn")
	public String deleteBySsn(@RequestParam(value="ssn", required=false) String ssn){
		
		return employeeService.deleteEmployeeBySsn(ssn);
	}
	@RequestMapping("/update")
	public String updateBySsn(
			@RequestParam(value="name", required=false) String name, 
			@RequestParam(value="salary", required=false) Double salary, 
			@RequestParam(value="ssn", required=false) String ssn){
		
		//Le enviamos un ssn que conozcamos pues no se repite, el id se omite.
		Employee employee = new Employee();
		employee.setName(name);
		employee.setSalary(salary);
		employee.setJoiningDate(LocalDate.now());
		employee.setSsn(ssn);
		return employeeService.updateEmployee(employee);
	}
}

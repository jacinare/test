package com.practica.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practica.model.Oficina;
import com.practica.service.OficinaService;

@RestController
@RequestMapping("/oficina")
public class OficinaController {
	
	@Autowired
	private OficinaService oficinaService;
	
	@RequestMapping("/findAll")
	public List<Oficina> findAllOficina(){
		return oficinaService.findAllOficina();
	}
	
	@RequestMapping(value="/save", method=RequestMethod.GET)
	public String saveOficina(
			@RequestParam(value="name", required=false) String name,
			@RequestParam(value="adress", required=false) String adress,
			@RequestParam(value="postCode", required=false) String postCode){
		
		Oficina oficina = new Oficina();
		oficina.setName(name);
		oficina.setAdress(adress);
		oficina.setPostCode(postCode);
		oficina.setCreationDate(LocalDate.now());
		return oficinaService.saveOficina(oficina);
	}

	@RequestMapping("/deleteByName")
	public String deleteByName(@RequestParam(value="name",required=false) String name){
		
		return oficinaService.deleteOficinaByName(name);
	}
	
	@RequestMapping(value="/update")
	public String updateOficinaById(
			@RequestParam(value="id",required=false) Long id,
			@RequestParam(value="name", required=false) String name,
			@RequestParam(value="adress", required=false) String adress,
			@RequestParam(value="postCode", required=false) String postCode){
		Oficina oficina = new Oficina();
		oficina.setId(id);
		oficina.setName(name);
		oficina.setAdress(adress);
		oficina.setPostCode(postCode);
		oficina.setCreationDate(LocalDate.now());
		return oficinaService.updateOficina(oficina);
	}
}

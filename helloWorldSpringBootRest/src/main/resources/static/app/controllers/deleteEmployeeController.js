app.controller('deleteEmployeeController', function($scope, $http, $modalInstance, id) {
	
	var deleteByIdSuccess = function(response) {
        console.log ("Delete success.");
        $modalInstance.close();
    };
    
    var deleteByIdError = function(response) {
    	$scope.errorMessage = "Codigo de error = " + response.status + ". El empleado no se pudo borrar.";
    	$modalInstance.close();
    };
    
    $scope.cancelar = function() {
    	$modalInstance.dismiss('cancel');
    }
	
	$scope.borrar = function() {
		$http.delete("/employee/deleteById/" + id).then(deleteByIdSuccess, deleteByIdError);
	};
	
	
});
var app = angular.module('myApp', ['ui.grid', 'ui.grid.pagination', 'ui.grid.selection', 'ui.bootstrap']);


app.controller('myCtrl', function($scope, $http, $modal) {
	
	$scope.firstName = "Comeme";
	$scope.lastName = "los webo";
	
	$scope.putaGridOptions = {
			// enablePagination: true,
			enableFiltering: true,
			multiSelect:false,
			paginationPageSizes: [5, 10, 20],
		    paginationPageSize: 5,
		    onRegisterApi: function(gridApi) {
		    	$scope.gridApi = gridApi;
		    }
	};
	
	var findAllSuccess = function(response) {
        $scope.putaGridOptions.data = response.data;
    };
    
    var findAllError = function(response) {
    	$scope.errorMessage = "Codigo de error = " + response.status + ". La ruta " + response.data.path + " es una basura. Su puta madre!!!";
    };
    
    var closeModalSuccess = function() {
    	$http.get("/employee/findAll").then(findAllSuccess, findAllError);
    };
    
    var closeModalError = function() {
    	console.log("El modal no se ha podido cerrar.");
    };
	
    // Esta llamada se hace al cargar la página por primera vez
	$http.get("/employee/findAll").then(findAllSuccess, findAllError);
	
	/* $http.get("/employee/finrrrdAll") .success(findAllSuccess).error(findAllError); */
	
	$scope.deleteEmployee = function() {
		if ($scope.gridApi.selection.getSelectedRows().length === 0) {
			console.log("No rows selected");
		
			$modal.open({
	            templateUrl: 'noRowsSelected-dialog.html',
	            controller: 'noRowsSelectedController',
	            size: 'sm',
	            backdrop: 'static'
	        });
		
		} else {
			var employeeId = $scope.gridApi.selection.getSelectedRows()[0].id;
			console.log("Id of selected employee " + $scope.gridApi.selection.getSelectedRows()[0].id);
			
			var modalInstance = $modal.open({
					                templateUrl: 'deleteEmployee-dialog.html',
					                controller: 'deleteEmployeeController',
					                size: 'sm',
					                backdrop: 'static',
					                resolve: {
					                    id: function () {
					                    	return employeeId;
					                    }
					                }
					            });
			
			modalInstance.result.then(closeModalSuccess, closeModalError);

		}
    };
	
});